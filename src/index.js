import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Header from './components/Header';
import Products from './components/Products';
import './scss/style.scss';

class App extends Component {
  constructor() {
    super();
    // Estado del componente raiz
    this.state = {
      categories: [],
      products: [],
      cart: (!localStorage.getItem("cart")) ? [] : JSON.parse(localStorage.getItem("cart")),
      totalItems: (!localStorage.getItem("totalItems")) ? 0 : localStorage.getItem("totalItems"),
      totalAmount: (!localStorage.getItem("totalAmount")) ? 0 : localStorage.getItem("totalAmount"),
      currentOrder: 'none',
      term: '',
      category: '',
      cartBounce: false,
      quantity: 1,
      modalActive: false,
      filter: '',
      searchVisible: false,
      priceMinFilter: null,
      priceMaxFilter: null,
      availableFilter: true,
      notAvailableFilter: true,
      quantityFilter: null,
      resetInputs: false
    };
    this.productsFile = require('../data/products.json');
    this.categoriesFile = require('../data/categories.json');
    this.cleanCart = this.cleanCart.bind(this);
    this.sumTotalItems = this.sumTotalItems.bind(this);
    this.sumTotalAmount = this.sumTotalAmount.bind(this);
    this.checkProduct = this.checkProduct.bind(this);
    this.updateQuantity = this.updateQuantity.bind(this);
    this.handleRemoveProduct = this.handleRemoveProduct.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    // Handlers
    this.handleSearch = this.handleSearch.bind(this);
    this.handleResetSearch = this.handleResetSearch.bind(this);
    this.handleFilterById = this.handleFilterById.bind(this);
    this.handleAddToCart = this.handleAddToCart.bind(this);
    this.handlePriceOrder = this.handlePriceOrder.bind(this);
    this.handleAvailableOrder = this.handleAvailableOrder.bind(this);
    this.handleQuantityOrder = this.handleQuantityOrder.bind(this);
    this.handleResetFilter = this.handleResetFilter.bind(this);
    this.handlePriceFilter = this.handlePriceFilter.bind(this);
    this.handleAvailableFilter = this.handleAvailableFilter.bind(this);
    this.handleQuantityFilter = this.handleQuantityFilter.bind(this);
  }

  

  /**
   * Función del ciclo. Carga los productos y catgorias del archivos json
   */
  componentWillMount() {
    this.loadCategories();
    this.loadProducts();
  }

  loadProducts() {
    this.setState({
      products: this.productsFile.products,
    },);
  }

  loadCategories() {
    this.setState({
      categories: this.categoriesFile.categories,
    });
  }

  /** Limpia el contenido del carrito por compra realizada */
  cleanCart() {
    localStorage.removeItem("cart");
    localStorage.removeItem("totalItems");
    localStorage.removeItem("totalAmount");
    this.setState({
      cart: [],
      totalItems: 0,
      totalAmount: 0
    });
  }

  /**
   * Formatea el valor de los productos
   */
  formatToNumber(value) {
    return parseFloat(value.substring(1).replace(",", "."));
  }

  /**
   * Filtra por precio de acuerdo al campo que corresponda
   * @param {*} price 
   * @param {*} isMax 
   */
  handlePriceFilter(price, isMax) {
    if (isMax) this.setState({ priceMaxFilter: price, resetInputs: false });
    else this.setState({ priceMinFilter: price, resetInputs: false });
  }

  /**
   * Filtra de acuerdo al estado del producto
   * @param {*} value 
   * @param {*} isAvailable 
   */
  handleAvailableFilter(value, isAvailable) {
    if (isAvailable) this.setState({ resetInputs: false, availableFilter: (value === 'on' && !this.state.availableFilter) ? true : false });
    else this.setState({ resetInputs: false, notAvailableFilter: (value === 'on' && !this.state.notAvailableFilter) ? true : false });
  }

  /**
   * Filtra de acuerdo a la cantidad de stock requerida
   * @param {*} value 
   */
  handleQuantityFilter(value) {
    this.setState({ quantityFilter: value, resetInputs: false });
  }

  /**
   * Filtrar por contenido
   * @param {*} event 
   */
  handleSearch(event) {
    this.setState({ term: event.target.value });
  }

  /**
   * Resetea busqueda por contenido
   */ 
  handleResetSearch() { 
    this.setState({ term: '' });
  }

  /**
   * Resetea filtros
   */
  handleResetFilter() {
    this.setState({ 
      filter: '',
      priceMinFilter: null,
      priceMaxFilter: null,
      availableFilter: true,
      notAvailableFilter: true,
      quantityFilter: null,
      searchVisible: false,
      resetInputs: true
    });
  }

  /**
   * Filtro por identificador de categoria
   * @param {*} id 
   * @param {*} hasSublevel 
   */
  handleFilterById(id, hasSublevel) {
    this.setState({ filter: id, searchVisible: !hasSublevel });
  }

  /**
   * Agregar nuevos productos en el carrito de compras
   * @param {*} selectedProducts 
   */
  handleAddToCart(selectedProducts) {
    const { cart } = this.state;
    const cartItem = cart;
    const productID = selectedProducts.id;
    const productQty = selectedProducts.quantity;
    if (this.checkProduct(productID)) {
      const index = cartItem.findIndex(x => x.id === productID);
      cartItem[index].quantity = Number(cartItem[index].quantity) + Number(productQty);
      this.setState({
        cart: cartItem,
        finishOrder: false
      });
    } else {
      cartItem.push(selectedProducts);
    }
    this.setState({
      cart: cartItem,
      cartBounce: true,
    });
    setTimeout(
      () => {
        this.setState({
          cartBounce: false,
          quantity: 1,
        });
      },
      1000,
    );
    localStorage.setItem("cart", JSON.stringify(cartItem));
    this.sumTotalItems(cart);
    this.sumTotalAmount(cart);
  }

  /**
   * Borra productos del carrito de compras
   * @param {*} id 
   * @param {*} e 
   */
  handleRemoveProduct(id, e) {
    const { cart } = this.state;
    const index = cart.findIndex(x => x.id == id);
    cart.splice(index, 1);
    this.setState({
      cart,
    });
    localStorage.setItem("cart", JSON.stringify(cart));
    this.sumTotalItems(cart);
    this.sumTotalAmount(cart);
    e.preventDefault();
  }

  /**
   * Ordena el listado por precio
   * @param {*} greaterToMinor 
   */
  handlePriceOrder(greaterToMinor) {
    const { currentOrder, products } = this.state;
    let order = currentOrder;
    if (currentOrder !== 'gtm-price' && greaterToMinor) {
      products.sort((a, b) => { return this.formatToNumber(b.price) - this.formatToNumber(a.price)});
      order = 'gtm-price';
    } else if(currentOrder !== 'mtg-price' && !greaterToMinor) {
      products.sort((a, b) => { return this.formatToNumber(a.price) - this.formatToNumber(b.price)});
      order = 'mtg-price';
    }
    this.setState({ products: products, currentOrder: order });
  }
  
  /**
   * Ordena el listado por estado de productos(disponible, no disponible)
   * @param {*} greaterToMinor 
   */
  handleAvailableOrder(greaterToMinor) {
    const { currentOrder, products } = this.state;
    let order = currentOrder;
    if (currentOrder !== 'gtm-available' && greaterToMinor) {
      products.sort((a, b) => { return (b.available === a.available) ? 0 : b.available ? -1 : 1 });
      order = 'gtm-available';
    } else if(currentOrder !== 'mtg-available' && !greaterToMinor) {
      products.sort((a, b) => { return (b.available === a.available) ? 0 : a.available ? -1 : 1});
      order = 'mtg-available';
    }
    this.setState({ products: products, currentOrder: order });
  }

  /**
   * Ordena por cantidad/stock
   * @param {*} greaterToMinor 
   */
  handleQuantityOrder(greaterToMinor) {
    const { currentOrder, products } = this.state;
    let order = currentOrder;
    if (currentOrder !== 'gtm-quantity' && greaterToMinor) {
      products.sort((a, b) => { return b.quantity - a.quantity});
      order = 'gtm-quantity';
    } else if(currentOrder !== 'mtg-quantity' && !greaterToMinor) {
      products.sort((a, b) => { return a.quantity - b.quantity});
      order = 'mtg-quantity';
    }
    this.setState({ products: products, currentOrder: order });
  }

  /**
   * Control si el producto se encuentra en el carrito de compras
   * @param {*} productID 
   */
  checkProduct(productID) {
    const { cart } = this.state;
    return cart.some(item => item.id === productID);
  }

  /**
   * Suma el total de los elementos del carrito de compras
   */
  sumTotalItems() {
    let total = 0;
    const { cart } = this.state;
    total = cart.length;
    this.setState({
      totalItems: total,
    });
    localStorage.setItem("totalItems", total);
  }

  /**
   * Suma el total del costo de los elementos del carrito de compras
   */
  sumTotalAmount() {
    let total = 0;
    const { cart } = this.state;
    for (let i = 0; i < cart.length; i++) {
      total += this.formatToNumber(cart[i].price) * parseInt(cart[i].quantity);
    }
    this.setState({
      totalAmount: total,
    });
    localStorage.setItem("totalAmount", total);
  }

  /**
   * Actualiza la cantidad del producto
   * @param {*} qty 
   */
  updateQuantity(qty) {
    this.setState({
      quantity: qty,
    });
  }

  /**
   * Hace visible el contenedor del carrito de compras
   * @param {*} product 
   */
  openModal(product) {
    this.setState({
      quickViewProduct: product,
      modalActive: true,
    });
  }

  /**
   * Cierra el contenedor del carrito de compras
   */
  closeModal() {
    this.setState({
      modalActive: false,
    });
  }

  /**
   * Renderiza la vista de la aplicación
   */
  render() {
    const { categories, cartBounce, searchVisible, priceMinFilter, priceMaxFilter, availableFilter, notAvailableFilter, quantityFilter,
      totalAmount, totalItems, cart, category, moq, products, term, filter, quantity, resetInputs } = this.state;
    return (
      <div className="container">
        <Header
            categoriesList={categories}
            cartBounce={cartBounce}
            cleanCart={this.cleanCart}
            total={totalAmount}
            totalItems={totalItems}
            cartItems={cart}
            categoryTerm={category}
            updateQuantity={this.updateQuantity}
            productQuantity={moq}
            searchVisible={searchVisible}
            removeProduct={this.handleRemoveProduct}
            resetInputs={resetInputs}
            handleSearch={this.handleSearch}
            handleResetSearch={this.handleResetSearch}
            handlePriceOrder={this.handlePriceOrder}
            handleAvailableOrder={this.handleAvailableOrder}
            handleQuantityOrder={this.handleQuantityOrder}
            handleResetFilter={this.handleResetFilter}
            handleFilterById={this.handleFilterById}
            handlePriceFilter={this.handlePriceFilter}
            handleAvailableFilter={this.handleAvailableFilter}
            handleQuantityFilter={this.handleQuantityFilter}
          />
        <Products
            productsList={products}
            addToCart={this.handleAddToCart}
            productQuantity={quantity}
            updateQuantity={this.updateQuantity}
            openModal={this.openModal}
            searchTerm={term}
            useFilter={filter}
            priceMinFilter={priceMinFilter}
            priceMaxFilter={priceMaxFilter}
            availableFilter={availableFilter}
            notAvailableFilter={notAvailableFilter}
            quantityFilter={quantityFilter}
            formatToNumber={this.formatToNumber}
          />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
