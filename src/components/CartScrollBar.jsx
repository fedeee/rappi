import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

/**
 * Componente encargado de administrar la barra de scroll del modal correspondiente al carro de compras
 */
export default class CartScrollBar extends Component {
  constructor(props) {
    super(props);
    this.handleScroll = this.handleScroll.bind(this);
  }

  // Funciones del ciclo de vida
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  // Funcion encargada de manejar el scroll
  handleScroll(event) {
    const positions = this.refs.scrollbars.getValues();
    // Previene que el scroll salga de su contenedor
    if (positions.top >= 1) {
      event.stopPropagation();
    }
  }

  render() {
    const { children } = this.props;
    return (
      <Scrollbars style={{ width: 360, height: 320 }} ref="scrollbars">
        {children}
      </Scrollbars>
    );
  }
}
