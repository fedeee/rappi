import React, { Component } from 'react';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import Product from './Product';
import ProductList from '../animation/Products';

/**
 * Componente correspondiente al listado de productos
 */
class Products extends Component {

  /**
   * Renderiza listado de productos(contenedor del listado)
   */
  render() {
    const { addToCart, productsList, productQuantity, updateQuantity, openModal, searchTerm, 
      useFilter, priceMinFilter, priceMaxFilter, availableFilter, notAvailableFilter, quantityFilter } = this.props;

    const productsData = productsList
      .filter(product => {
        return product.name.toLowerCase().includes(searchTerm.toLowerCase()) || !searchTerm;
      })
      .filter(product => { 
        if (useFilter === '' ) return true;
        if (product.sublevel_id === useFilter) return true;
        return false;
      })
      .filter(product => {
        if (!priceMinFilter && !priceMaxFilter) return true;
        if (priceMinFilter && !priceMaxFilter) return (Number(priceMinFilter) < this.props.formatToNumber(product.price));
        if (!priceMinFilter && priceMaxFilter) return (Number(priceMaxFilter) > this.props.formatToNumber(product.price)); 
        return (Number(priceMinFilter) <= this.props.formatToNumber(product.price) && this.props.formatToNumber(product.price) <= Number(priceMaxFilter)); 
      })
      .filter(product => {
        if (availableFilter && notAvailableFilter) return true;
        if (!availableFilter && notAvailableFilter) return (product.available);
        if (availableFilter && !notAvailableFilter) return (!product.available);
        return false;
      })
      .filter(product => {
        if (!quantityFilter) return true;
        return (product.quantity === Number(quantityFilter))
      })
      .map(product => (
        <Product
          key={product.id}
          price={product.price}
          name={product.name}
          available={product.available}
          stock={product.quantity}
          id={product.id}
          addToCart={addToCart}
          productQuantity={productQuantity}
          updateQuantity={updateQuantity}
          openModal={openModal}
        />
      ));

    // Empty and Loading States
    let view;
    if (productsData.length <= 0 && !searchTerm) {
      view = <ProductList />;
    } else if (productsData.length <= 0 && searchTerm) {
      view = (
        <div className="products">
          <div className="no-results">
            <h2>No tenemos productos que se adapten a su búsqueda!</h2>
          </div>
        </div>
      );
    } else {
      view = (
        <CSSTransitionGroup
          transitionName="fadeIn"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
          component="div"
          className="products"
        >
          {productsData}
        </CSSTransitionGroup>
      );
    }
    return <div className="products-wrapper">{view}</div>;
  }
}

export default Products;
