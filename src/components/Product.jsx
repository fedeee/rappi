import React, { Component } from 'react';
import Counter from './Counter';

/**
 * Componente que representa caja grafica de producto
 */
class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedProduct: {},
      isAdded: false,
    };
  }

  /**
   * Agrega un producto en el carro de compras
   * @param {*} name 
   * @param {*} price 
   * @param {*} id 
   * @param {*} quantity 
   */
  addToCart(name, price, id, quantity) {
    this.setState({
        selectedProduct: {
          name,
          price,
          id,
          quantity,
        },
      },
      () => {
        this.props.addToCart(this.state.selectedProduct);
      },
    );
    this.setState(
      {
        isAdded: true,
      },
      () => {
        setTimeout(() => {
          this.setState({
            isAdded: false,
            selectedProduct: {},
          });
        }, 3500);
      },
    );
  }

  /**
   * Renderiza el componente correspondiente al producto
   */
  render() {
    const { stock, available, name, price, id } = this.props;
    const quantity = this.props.productQuantity;
    return (
      <div className="product">
        <h4 className="product-name">{name}</h4>
        <p className="product-price">{price}</p>
        <p className="product-name">Stock: {stock}</p>
        <p className="product-name">Disponible: { (available) ? 'Si' : 'No' }</p>
        <Counter
          productQuantity={quantity}
          updateQuantity={this.props.updateQuantity}
          resetQuantity={this.resetQuantity}
        />
        <div className="product-action">
          {
            (available) ?
              (<button
                className={!this.state.isAdded ? '' : 'AGREGADO'}
                type="button"
                onClick={this.addToCart.bind(
                  this,
                  name,
                  price,
                  id,
                  quantity,
                )}
              >
                {!this.state.isAdded ? 'AGREGAR' : '✔ AGREGADO'}
              </button>)
            :
              (<button
                className="NODISPONIBLE"
                type="button"
                disabled={true}
              >
                AGREGAR
              </button>)
          }
          
        </div>
      </div>
    );
  }
}

export default Product;
