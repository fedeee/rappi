import React from 'react';

/**
 * Componente relacionado con el producto. Operador de cantidad
 */
class Counter extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { value: props.productQuantity };
    this.addup = this.addup.bind(this);
    this.subtract = this.subtract.bind(this);
  }

  // addup and subtract not working well
  addup(e) {
    const { updateQuantity } = this.props;
    const { value } = this.state;
    this.setState({ value: Number(value) + 1 },
      () => {
        updateQuantity(this.state.value);
      });
    e.preventDefault();
  }

  subtract(e) {
    const { updateQuantity } = this.props;
    const { value } = this.state;
    e.preventDefault();
    if (value <= 1) {
      return value;
    }
    this.setState({ value: Number(value) - 1 }, () => {
      updateQuantity(this.state.value);
    });
  }

  update() {
    const { updateQuantity } = this.props;
    const { value } = this.state;
    this.setState({ value: this.refs.updateQty.value }, () => {
      updateQuantity(value);
    });
  }

  resetQuantity() {
    this.setState({
      value: 1,
    });
  }

  render() {
    const { value } = this.state;
    return (
      <div className="stepper-input">
        <a href="#" className="subtract" onClick={this.subtract}>
        –
        </a>
        <input
          ref="updateQty"
          type="number"
          className="quantity"
          value={value}
          onChange={this.update.bind(this)}
        />
        <a href="#" className="addup" onClick={this.addup}>
        +
        </a>
      </div>
    );
  }
}

Counter.state = {
  value: 0
}

export default Counter;
