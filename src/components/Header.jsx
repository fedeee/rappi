import React, { Component } from 'react';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { findDOMNode } from 'react-dom';
import CartScrollBar from './CartScrollBar';
import classnames from 'classnames';
import cart from '../../assets/cart.png';
import back from '../../assets/back.png';
import search from '../../assets/search-black.png';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCart: false,
      cart: this.props.cartItems,
      mobileSearch: false,
      openSublevel: '',
      sublevels: []
    };
  }

  /**
   * Función del ciclo. Captura el evento del click fuera del modal para realizar la acción relacionada
   */
  componentDidMount() {
    document.addEventListener(
      'click',
      this.handleClickOutside.bind(this),
      true,
    );
  }

  /**
   * Actualiza componente de acuerdo a la actualización de propiedades
   * @param {*} newProps 
   */
  componentWillUpdate(newProps) {
    if (newProps.cartItems !== this.state.cart) this.setState({ cart: newProps.cartItems });
    if (newProps.resetInputs) this.resetInputs();
  }

  /**
   * Función del ciclo. Captura el evento del click fuera del modal para realizar la acción relacionada
   */
  componentWillUnmount() {
    document.removeEventListener(
      'click',
      this.handleClickOutside.bind(this),
      true,
    );
  }

  /**
   * Resetea a valores iniciales los elementos relacionados a los filtros
   */
  resetInputs() {
    this.minPriceInput.value = "";
    this.maxPriceInput.value = "";
    this.availableInput.checked = false;
    this.notAvailableInput.checked = false;
    this.quantityInput.value = "";
  }

  /**
   * Muestra o esconde carro de compras
   * @param {*} e 
   */
  handleCart(e) {
    e.preventDefault();
    this.setState({
      showCart: !this.state.showCart,
    });
  }

  /**
   * Realiza el submit de busqueda
   * @param {*} e 
   */
  handleSubmit(e) {
    e.preventDefault();
  }

  /**
   * Resetea la busqueda 
   */
  handleResetSearch(e) {
    e.preventDefault();
    this.setState({
      mobileSearch: true,
    });
  }

  handleSearchNav(e) {
    e.preventDefault();
    this.setState(
      {
        mobileSearch: false,
      },
      () => {
        this.refs.searchBox.value = '';
        this.props.handleResetSearch();
      },
    );
  }

  /**
   * Maneja el click fuera del modal correspondiente al carro de compras
   * @param {*} event 
   */
  handleClickOutside(event) {
    const cartNode = findDOMNode(this.refs.cartPreview);
    if (cartNode.classList.contains('active')) {
      if (!cartNode || !cartNode.contains(event.target)) {
        this.setState({
          showCart: false,
        });
        event.stopPropagation();
      }
    }
  }

  /**
   * Renderiza subniveles. Trabaja en conjunto con el metodo loadCategoryType
   * @param {*} category 
   */
  loadLevels(category) {
    if (!this.state.sublevels.length) return;
    return this.state.sublevels.map(category => {
      return (
      <div className="categoriesChild">
        <span className="next"> → </span>
        <span className="action" onClick={() =>this.props.handleFilterById(category.id, category.sublevels)}>{category.name}</span>
          {(category.sublevels) ? 
            (
            <span className={classnames("action more")} onClick={() =>this.setState({ openThirdlevel: category.id, sublevels: category.sublevels })}> + </span>
            ) : 
            null}
      </div>);
    });
  }

  /**
   * Renderiza subniveles de las categorias principales
   */
  loadCategoryType(category) {
    if (category.sublevels && this.state.openSublevel === '') return (<span className={classnames("action more")} onClick={() =>this.setState({ openSublevel: category.id, sublevels: category.sublevels })}> + </span>);
    if (this.state.openSublevel !== '' && this.state.openSublevel === category.id) return (this.loadLevels(category));
    else return null;
  }

  /**
   * Renderiza categorias de productos
   * @param {*} categoriesList 
   */
  loadCategories(categoriesList) {
    return categoriesList.map(category => {
      return (
      <div className="categoriesContainer">
        <span className={classnames("action less")} onClick={() =>this.setState({ openSublevel: '', sublevels: []})}> - </span>
        <span className="action" onClick={() =>this.props.handleFilterById(category.id, category.sublevels)}><strong>{category.name}</strong></span>
          { 
            this.loadCategoryType(category)
          }
      </div>);
    });
  }

  /**
   * Renderiza elemento de busqueda
   */
  loadSearchBox() {
    return (<form
      action="#"
      method="get"
      className={this.state.mobileSearch ? 'search-form active' : 'search-form'
      }
    >
      <a className="back-button" href="#" onClick={this.handleSearchNav.bind(this)}>
        <img
          src={back}
          alt="back"
        />
      </a>
      <input
        type="search"
        ref="searchBox"
        placeholder="Buscar"
        className="search-keyword"
        onChange={this.props.handleSearch}
      />
      <button
        className="search-button"
        type="submit"
        onClick={this.handleSubmit.bind(this)}
      />
    </form>);
  }

  /**
   * Formatea el valor de los productos
   */
  formatToNumber(value) {
    return parseFloat(value.substring(1).replace(",", "."));
  }

  /**
   * Renderiza la vista del componente
   */
  render() {
    const cartItems = this.state.cart.map(product => (
      <li className="cart-item" key={product.name}>
        <div className="product-info">
          <p className="product-name">{product.name}</p>
          <p className="product-price">{product.price}</p>
        </div>
        <div className="product-total">
          <p className="quantity">
            {product.quantity}
            {' '}
            {'Nro.'}
            {' '}
          </p>
          <p className="amount">{product.quantity * this.formatToNumber(product.price)}</p>
        </div>
        <a className="product-remove" href="#" onClick={this.props.removeProduct.bind(this, product.id)}>
          ×
        </a>
      </li>
    ));
    let view;
    if (cartItems.length <= 0) {
      view = (
        <div className="empty-cart">
          <br />
          <h2>Tu carro está vacío</h2>
        </div>
      );
    } else {
      view = (
        <CSSTransitionGroup
          transitionName="fadeIn"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
          component="ul"
          className="cart-items"
        >
          {cartItems}
        </CSSTransitionGroup>
      );
    }
    return (
      <header>
        <div className="container">
          <div className="brand">
            <strong>El Baratón</strong>
          </div>
          <div className="search">
            {
              (this.props.searchVisible) ?
              <a className="mobile-search" href="#" onClick={this.handleResetSearch.bind(this)}>
                <img
                  src={search}
                  alt="search"
                />
              </a>
              : ''
            }
            {
            (this.props.searchVisible) ?
              this.loadSearchBox()
            : ''
            }
            {
              this.loadCategories(this.props.categoriesList)
            }
            <div className={classnames("actionContainer order")}>
              <div className="block">Precio <span className="arrow" onClick={() => this.props.handlePriceOrder(true)}>▲</span><span className="arrow" onClick={() => this.props.handlePriceOrder(false)}>▼</span></div>
              <div className="block">Disponibilidad <span className="arrow" onClick={() => this.props.handleAvailableOrder(true)}>▲</span><span className="arrow" onClick={() => this.props.handleAvailableOrder(false)}>▼</span></div>
              <div className="block">Cantidad <span className="arrow" onClick={() => this.props.handleQuantityOrder(true)}>▲</span><span className="arrow" onClick={() => this.props.handleQuantityOrder(false)}>▼</span></div>
            </div>
            <div className="actionContainer">
              <div className={classnames("block left")}>Precio 
              <input type="text" size="3" ref={node => { this.minPriceInput = node; }} onChange={evt => this.props.handlePriceFilter(evt.target.value, false)} /> &lt; 
              <input type="text" size="3" ref={node => { this.maxPriceInput = node; }} onChange={evt => this.props.handlePriceFilter(evt.target.value, true)} /></div>
              <div className="block"> Disponible 
              <input type="checkbox" ref={node => { this.availableInput = node; }} onChange={evt => this.props.handleAvailableFilter(evt.target.value, true)}/> No disponible 
              <input type="checkbox" ref={node => { this.notAvailableInput = node; }} onChange={evt => this.props.handleAvailableFilter(evt.target.value, false)}/> </div>
              <div className={classnames("block right")}>Cantidad 
              <input type="text" size="3" ref={node => { this.quantityInput = node; }} onChange={evt => this.props.handleQuantityFilter(evt.target.value)
                }/>
              </div>
            </div>
            <div className="actionContainer">
              <span className="arrow" onClick={() => this.props.handleResetFilter()}><strong>Limpiar filtros</strong></span>
            </div>
          </div>

          <div className="cart">
            <div className="cart-info">
              <table>
                <tbody>
                  <tr>
                    <td>Productos</td>
                    <td>:</td>
                    <td>
                      <strong>{this.props.totalItems}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>Subtotal</td>
                    <td>:</td>
                    <td>
                      <strong>{this.props.total}</strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <a className="cart-icon" href="#" onClick={this.handleCart.bind(this)} ref="cartButton">
              <img
                className={this.props.cartBounce ? 'tada' : ' '}
                src={cart}
                alt="Cart"
              />
              {this.props.totalItems ? (
                <span className="cart-count">{this.props.totalItems}</span>
              ) : (
                ''
              )}
            </a>
            <div
              className={
                this.state.showCart ? 'cart-preview active' : 'cart-preview'
              }
              ref="cartPreview"
            >
              <CartScrollBar>{view}</CartScrollBar>
              <div className="action-block">
                <button
                  type="button"
                  className={this.state.cart.length > 0 ? ' ' : 'disabled'}
                  onClick={() => {
                    this.setState({ showCart: false }, () => this.props.cleanCart());
                  }}
                >
                  CERRAR PEDIDO
                </button>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
