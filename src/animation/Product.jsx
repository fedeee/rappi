import React, { Component } from "react";

const Product = () => {
  return (
    <div className="product loading">
      <div className="product-image" />
      <div className="product-text" />
      <div className="product-text" />
      <div className="product-text" />
      <div className="product-button" />
    </div>
  );
};

export default Product;