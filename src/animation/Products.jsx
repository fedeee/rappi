import React from 'react';
import Product from './Product';

const ProductList = () => (
  <div className="products loading">
    <Product />
    <Product />
    <Product />
    <Product />
    <Product />
    <Product />
    <Product />
    <Product />
  </div>
);

export default ProductList;
