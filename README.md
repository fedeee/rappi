# Carro de compras 'El Baraton'

## Descripción de la aplicación

La tienda virtual cuenta con diferentes acciones para hacer la interacción del usuario muy amigable.
Partiendo de la consigna y requerimientos solicitados en el pdf. 
La misma provee categorías de productos las cuales toman acción de filtro si se hace click sobre sus nombres. 
Acompañando las categorías en el sector superior, se cuenta con opciones de ordenamiento y filtros especificos. Relacionados con el precio, unidades y estado de los productos(disponible o no disponible).
En caso de llegar a una categoría hoja, la cual no posee subniveles, aparecerá visible en el sector superior un buscador por contenido(nombre). El cual permitirá buscar en los productos visibles del catalogo.

El foco de la misma está en la comodidad del usuario para usarla, brindando animaciones  que resultan agradables para el ojo humano y colores caracteristicos de "Rappi". 
El catalogo permite editar las cantidades de productos para agregar al carro de compras, que será visible si se hace click sobre el icono del mismo en el sector superior derecho. En caso de no desear un producto agregado al carro, puede eliminarse el mismo. 
Qué sucede si el navegador se cierra por X motivo? No hay problema! La tienda conserva el estado de tu carro de compras. 

ACLARACIÓN: Las comas en los valores de los productos no sabian como interpretarse. Si como miles, o como decimales. Se considero que son decimales, por lo tanto en el carro de compras son reemplazados por puntos.

Al hacer la compra, la aplicación limpiará por completo el carrito de compras.

## Tecnologias utilizadas

La biblioteca principal utilizada para crear la tienda, ha sido React. Asociada fuertemente con Javascript. La misma ha sido diseñada para crear interfaces de usuario con el objetivo de facilitar el desarrollo de aplicaciones de una sola página(SPA).
Junto con React, se ha elegido SASS como preprocesador de hojas de estilo. Un preprocesador de hoja de estilo, de CSS, es una herramienta que nos permite escribir pseudo-código CSS que luego será convertido a CSS real. Ese pseudo-código se conforma de variables, condiciones, bucles o funciones.
En conjunto con React, SASS, JS se han utilizado diferentes paquetes que nutren la experiencia en la aplicación. 
Quizás en el archivo package figuren más paquetes que los utilizados. Tomamos eso como fe de errata. Por no limpiar aquellos que se ha desistido su uso. 
Finalmente, vale la pena nombrar a quien hace que todos se conjugue a la perfección. Quién es él? Su nombre es Webpack, un sistema de bundling para preparar el desarrollo de una aplicación web. Asociado siempre con React. Se puede considerar una evolución de Grunt y Gulp, ya que permite de alguna manera automatizar los procesos principales que son transpilar y preprocesar código de .scss a .css, de ES7 a ES5/6, etc.

Este trío hace posible la aplicación lograda.

## Descripción del desarrollo + Buenas practicas

Introduzcamos brevemente React JS, para que sepamos como funciona.

React.js está construido en torno a hacer funciones, que toman las actualizaciones de estado de la página y que se traduzcan en una representación virtual de la página resultante. Siempre que React es informado de un cambio de estado, vuelve a ejecutar esas funciones para determinar una nueva representación virtual de la página, a continuación, se traduce automáticamente ese resultado en los cambios del DOM necesarios para reflejar la nueva presentación de la página.

A primera vista, esto suena como que fuera más lento que el enfoque JavaScript habitual de actualización de cada elemento, según sea necesario. Pero detrás de escena, sin embargo, React tiene un algoritmo muy eficiente para determinar las diferencias entre la representación virtual de la página actual y la nueva. A partir de esas diferencias, hace el conjunto mínimo de cambios necesarios en el DOM.

Pues utiliza un concepto llamado el DOM virtual que hace selectivamente sub-árboles de los nodos sobre la base de cambios de estado, desarrollando esto, con la menor cantidad de manipulación DOM posible, con el fin de mantener los componentes actualizados, estructurando sus datos. Produciendo un resultado excelente.

La aplicación fue centrada en React de forma individual, basandose en sus cambios de estados y propiedades. Creando componente para representar graficamente la tienda deseada. 

Es la practica deseada? 

Si bien se ha tomado la opción de mostrar el manejo de estados y propiedades de React de forma pura.
Lamentablemente, no es la mejor practica. Si bien cumple con lo requerido, lo ideal es manejar la asincronia con Redux.

Qué nos brinda Redux? 

Redux es una librería para controlar el estado de nuestras aplicaciones web fácilmente. Se encarga de emitir actualizaciones de estado en respuesta a acciones. En lugar de modificar directamente el estado, la modificación se maneja a través de objetos sencillos llamados acciones. Luego existen reductores para decidir cómo cada acción transforma el estado de toda la aplicación. Y por último todo se maneja desde un solo lugar llamado store. Ya que la repetición ayuda a recordar las cosas podemos decir que los elementos principales de esta arquitectura o patron de diseño son:

- Acciones
- Reducers
- Store

La librería Redux en sí es sólo un conjunto de ayudas para “montar” reductores a un único objeto de store global. De forma de manejar más limpiamente y unificada el estado general de la aplicación. 

Veamos brevemente como sería el flujo de la aplicación utilizando Redux. 

Los componentes ante alguna acción realizada por el usuario, pueden desear cambiar el estado para renderizar un nuevo elemento o cambio.
Qué debe hacer ante eso? El mismo componente, estará asociado a propiedades que se encargaran de disparar las acciones que procedan a ser capturadas por el reductor correspondiente, quien finalmente cambiará el estado para que dicho componente pueda mostrarse actualizado.

Es bueno asociar con cada componente un contenedor. 

```javascript
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { productState } from '../state-mgmt/rootState'; // { productState, coreState, ... }
import Actions from '../../actions/index';
import Product from '../components/Product'; // Componente asociado al contenedor

const mapDispatchToProps = dispatch => ({
    isLoading: state.product.isLoading, // Mapea propiedades del estado a props del componente
    hasError: state.product.hasError
});

const mapStateToProps = state => ({
    changeProductName: name => dispatch(productState.actions.changeName(name)),
    checkForUpdates: () => dispatch(productState.actions.checkForUpdates())
});

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Counter);
```

De dicha forma el contenedor será el encargado de disparar la acción que se desee para cambiar el estado. 
Imaginemos que hay que agregar un producto. La acción asociada será ADD_PRODUCT

```javascript
var action = {
  type: 'ADD_PRODUCT',
  product: {name: 'Televisor LG'}
};

// Action type: Tipo de accion
const ADD_PRODUCT = 'ADD_PRODUCT';

// Action Creator: Creador de la accion
var addProduct = function(name) {
  return { type: ADD_PRODUCT, name}
}

// Dispatch: Disparador de accion
store.dispatch(addProduct('Ipod'));
```

Una vez disparada la acción, debe ser capturada por un reductor. Que procederá a partir de la misma y el estado de la aplicacion actual, devolver el nuevo estado.
```javascript 
(previousState, action) => newState
```
El reductor debe ser puro. Dado los mismos argumentos, debes calcular el siguiente estado y devolverlo.

```javascript
import { combineReducers } from 'redux'
import { ADD_PRODUCT } from './actions'

function products(state = [], action) {
  switch (action.type) {
    case ADD_PRODUCT:
      return [
        ...state,
        {
          name: action.name
        }
      ]
    default:
      return state
  }
}

const reducers = combineReducers({
  products
})

export default reducers
```

Dichos reductores, sea el del ejemplo y otros que representen reductores de diferentes componentes de la aplicación será quien brinden el nuevo estado al elemento que conjuga el resultado final. El store.

Como hemos visto las acciones representan los hechos sobre “lo que pasó” y los reductores actualizan el estado de acuerdo a esas acciones. El store es el objeto que los reúne. El objeto store, almacena el estado de aplicación, permite el acceso al estado a través del método getState(), permite que el estado se actualice a través del método dispatch(action) y registra los listeners mediante el método subscribe(listener).

Cómo se provee el estado a la aplicación? En el elemento raíz, 

```javascript
import { Provider } from 'react-redux';
import { store } from './store';

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
```

En caso de haberse tomado esta orientación, para persistir el estado del carro de compras se podría haber importado la función persistReducer de redux.

Qué puede utilizarse de la mano de Redux, Thunk o Sagas. Pero dejemos esos temas para charlar más profundamente de forma personal.

## TODO

Cambios faltantes deseados? Sí, pequeños cambios esteticos que pueden mejorar la calidad visual de la tienda y mayor modularización de metodos y componentes. Componentes más pequeños aumentan la reusabilidad de los mismos. 

## Setup

Ir a la raiz principal del proyecto e instalar las siguientes dependencias:

```
npm install
```

Y luego correr webpack para cambios de codigo(modo desarrollo) y armar el bundle js y los archivos de estilo:

```
npm start
```

El proyecto correrá en el puerto 8015. Determinado en el archivo webpack.
URL: http://localhost.com:8015
